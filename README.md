# randomMusicKeySelector

This is a simple little application to randomly give you a key and scale to use as a start for a song. 
Also spits out the notes for it. If you want to add a scale, simply add an entry to the scales slice
with the scale name and the intervals between notes in semitones.

Feel free to make pull requests if you want any scales added. Also please note which scales are available 
in this readme too, please! Scales are listed below.

* Major
* Minor
* Blues Major
* Blues Minor
* Pentatonic Major
* Pentatonic Minor
* Dorian
* Lydian
* Phrygian
* Mixolydian
* Locrian

# Build instructions

To build, install the Go compiler (found at https://go.dev), and download the file randomMusicKeySelector.go
(or clone the repo if you know how this works already). From there, open a terminal to wherever the
randomMusicKeySelector.go is located and run the command `go build randomMusicKeySelector.go`.

Then just run the output file, and you're good to go!