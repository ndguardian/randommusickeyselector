package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Scale struct {
	name                  string
	incrementsInSemitones []int
}

var (
	notes = []string{
		"C",
		"C#",
		"D",
		"D#",
		"E",
		"F",
		"F#",
		"G",
		"G#",
		"A",
		"A#",
		"B",
	}

	scaleIntervals = []Scale{
		{"Major", []int{2, 2, 1, 2, 2, 2}},
		{"Minor", []int{2, 1, 2, 2, 1, 2}},
		{"Blues Major", []int{2, 1, 1, 3, 2}},
		{"Blues Minor", []int{3, 2, 1, 1, 3}},
		{"Pentatonic Major", []int{2, 2, 3, 2}},
		{"Pentatonic Minor", []int{3, 2, 2, 3}},
		{"Dorian", []int{2, 1, 2, 2, 2, 1}},
		{"Lydian", []int{2, 2, 2, 1, 2, 2}},
		{"Phrygian", []int{1, 2, 2, 2, 1, 2}},
		{"Mixolydian", []int{2, 2, 1, 2, 2, 1}},
		{"Locrian", []int{1, 2, 2, 1, 2, 2}},
	}
)

func main() {
	rand.Seed(time.Now().UnixNano())
	randomKeyIndex := rand.Intn(len(notes))
	randomScaleIndex := rand.Intn(len(scaleIntervals))

	fmt.Println("Your randomly chosen scale is:", notes[randomKeyIndex], scaleIntervals[randomScaleIndex].name)
	notesInKey := getNotesInKeyScaleCombo(randomKeyIndex, notes, scaleIntervals[randomScaleIndex].incrementsInSemitones)
	fmt.Println("The notes in key are:", notesInKey)
	chordPattern := getRandomFourChordPatternFromScale(len(scaleIntervals[randomScaleIndex].incrementsInSemitones))
	fmt.Println("And may I recommend this chord pattern to start with?", chordPattern)
}

func getNotesInKeyScaleCombo(startingNoteIndex int, notesSlice []string, intervals []int) []string {
	currentPosition := startingNoteIndex
	var outputSlice []string
	outputSlice = append(outputSlice, notesSlice[currentPosition])

	for _, interval := range intervals {
		currentPosition += interval
		if currentPosition >= len(notesSlice) {
			currentPosition = currentPosition - len(notesSlice)
		}
		outputSlice = append(outputSlice, notesSlice[currentPosition])
	}

	return outputSlice
}

func getRandomFourChordPatternFromScale(notesInScaleCount int) []int {
	var chordPattern []int
	for i := 0; i < 4; i++ {
		chordPattern = append(chordPattern, rand.Intn(notesInScaleCount+1)+1)
	}

	return chordPattern
}
